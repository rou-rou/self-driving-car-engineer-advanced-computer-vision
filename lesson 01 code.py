# 定位车道线
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

# 加载我们的图片
# 归一化处理
img = mpimg.imread('warped-example.jpg')/255

def hist(img):
    # 只看图片的下半部分
    # 线垂直于汽车
    bottom_half = img[img.shape[0]//2:,:]

    histogram = np.sum(bottom_half, axis=0)
    
    return histogram

# 创建图像二进制激活直方图
histogram = hist(img)

# 可视化
plt.plot(histogram)
plt.show()