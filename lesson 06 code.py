# 测量曲率

import numpy as np

def generate_data():
    '''
    将上面代码生成的曲率的虚假数据用来生成车道曲率
    '''
    # 设置一个随机的种子数 以便分级器结果一致
    # 如果你想查看不同随机数据结果 就把他注释掉
    np.random.seed(0)

    # -------------------------生成假数据代码-------------------------
    ploty = np.linspace(0, 719, num=720)#
    quadratic_coeff = 3e-4 

    leftx = np.array([200 + (y**2)*quadratic_coeff + np.random.randint(-50, high=51) 
                                    for y in ploty])
    rightx = np.array([900 + (y**2)*quadratic_coeff + np.random.randint(-50, high=51) 
                                    for y in ploty])

    leftx = leftx[::-1]
    rightx = rightx[::-1]

    left_fit = np.polyfit(ploty, leftx, 2)
    right_fit = np.polyfit(ploty, rightx, 2)
    
    return ploty, left_fit, right_fit
    
def measure_curvature_pixels():
    '''
    计算曲率
    '''
    # 传入方法生成假数据
    ploty, left_fit, right_fit = generate_data()
    
    # 定义曲率半径的y值 选择最大的y值对应于图像的底部
    y_eval = np.max(ploty)
    
    # 曲率半径计算
    left_curverad = ((1 + (2 * left_fit[0] * y_eval + left_fit[1]) ** 2) ** 1.5) / np.absolute(2 * left_fit[0])
    right_curverad = ((1 + (2 * right_fit[0] * y_eval + right_fit[1]) ** 2) ** 1.5) / np.absolute(2 * right_fit[0])
    
    return left_curverad, right_curverad


# 输入方法 计算两条车道线的曲率半径
left_curverad, right_curverad = measure_curvature_pixels()

print("%.2f" % left_curverad)
print("%.2f" % right_curverad)
